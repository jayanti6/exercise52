function DropDownMenuCreator(domElements){
  this.lists = domElements.unorderedListItems;
}

DropDownMenuCreator.prototype.bindHoverToNavs = function(){
  this.lists.has("ul").hover(function(){
    $(this).toggleClass("hover").find("ul").slideToggle();
  });
};

DropDownMenuCreator.prototype.init = function(){
  this.bindHoverToNavs();
};

$(document).ready(function(){
  var domElements = {
    unorderedListItems: $("ul#nav").find("li")
  };

  var dropDownMenuCreator = new DropDownMenuCreator(domElements);
  dropDownMenuCreator.init();
});